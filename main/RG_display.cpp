/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "../ITEADLIB_Arduino_Nextion/Nextion.h"
#include <string.h>

const int standby_page_id = 0;
const int numpad_page_id = 1;
const int wrong_pin_page_id = 2;
const int balance_page_id = 3;

void resetPasswordInput();

// Declare your Nextion objects - Example (page id = 0, component id = 1, component name = "b0") 

// pages
NexPage standby_page = NexPage(standby_page_id, 0, "standby");
NexPage numpad_page = NexPage(numpad_page_id, 0, "numpad");
NexPage wrong_pin_page = NexPage(wrong_pin_page_id, 0, "wrong_pin");
NexPage balance_page = NexPage(balance_page_id, 0, "balance");

// Entities on numpad page
NexText tPassword = NexText(numpad_page_id, 14, "tPassword"); 
NexButton b1 = NexButton(numpad_page_id, 1, "b1");
NexButton b2 = NexButton(numpad_page_id, 2, "b2");
NexButton b3 = NexButton(numpad_page_id, 3, "b3");
NexButton b4 = NexButton(numpad_page_id, 4, "b4");
NexButton b5 = NexButton(numpad_page_id, 5, "b5");
NexButton b6 = NexButton(numpad_page_id, 6, "b6");
NexButton b7 = NexButton(numpad_page_id, 7, "b7");
NexButton b8 = NexButton(numpad_page_id, 8, "b8");
NexButton b9 = NexButton(numpad_page_id, 9, "b9");
NexButton b0 = NexButton(numpad_page_id, 11, "b0");
NexButton bAsterisk = NexButton(numpad_page_id, 10, "bAsterisk");
NexButton bNumberSign = NexButton(numpad_page_id, 11, "bNumberSign");
NexButton bDelete = NexButton(numpad_page_id, 13, "bDelete"); 

// Entities on wrong_pin page
//NexText tRemainTries = NexText(wrong_pin_page_id, 1, "tRemainTries"); 
NexNumber nRemainTries = NexNumber(wrong_pin_page_id, 1, "nRemainTries");
// Register a button object to the touch event list.
NexTouch *nex_listen_list[] = {
  &standby_page,
  &numpad_page,
  &wrong_pin_page,
  &balance_page,
  &b1,
  &b2,
  &b3,
  &b4,
  &b5,
  &b6,
  &b7,
  &b8,
  &b9,
  &b0,
  &bAsterisk,
  &bNumberSign,
  &bDelete,
  NULL
};



// die laenge des array sind anzahl chars + 1 wegen NULL am Ende
// define global char array with four spaces
char password[5] = "    ";

const char real_password[5]  = "1234";

// initialise len of char array password
unsigned int counter = 0; 

unsigned int maximum_tries = 5;
unsigned int remaining_tries = 5;

void numpadButton(char symbol)
{
 /*
 * Text aus tPassword nehmen
 * tPassword mit real_password abgleichen
 * wenn es stimmt: Guthabenseite anzeigen
 * wenn es nicht stimmt: Fail anzeigen und counter für tries hochzählen
 */

//
//  if (counter > 4){
//    return;
//  }
  password[counter] = symbol;
  tPassword.setText(password);
  counter++;

  if (counter == 4 && strcmp(password,real_password) == 0 ){
    resetPasswordInput();
    balance_page.show();
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    // delay(2000);
    standby_page.show();
    remaining_tries = maximum_tries;
    return;
  }
  else if (counter == 4 && strcmp(password,real_password) != 0 ){
    resetPasswordInput();
    wrong_pin_page.show();
    //char remaining_tries_string[1];
    //remaining_tries_string[0] = char(remaining_tries);
    //char remaining_tries_char[] = {char(remaining_tries)};
    
    
    nRemainTries.setValue(remaining_tries);

    if (remaining_tries == 0) {
      remaining_tries = 0;
    }
    else {
      remaining_tries--;
    }
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    // delay(2000);
    numpad_page.show();
    return;
  }
}

void b1PopCallback(void *ptr)
{
  // digitalWrite(LED_BUILTIN, HIGH);
  numpadButton('1');
}

void b2PopCallback(void *ptr)
{
  // digitalWrite(LED_BUILTIN, LOW);
  numpadButton('2');
}

void b3PopCallback(void *ptr)
{
  numpadButton('3');
}

void b4PopCallback(void *ptr)
{
  numpadButton('4');
}

void b5PopCallback(void *ptr)
{
  numpadButton('5');
}

void b6PopCallback(void *ptr)
{
  numpadButton('6');
}

void b7PopCallback(void *ptr)
{
  numpadButton('7');
}

void b8PopCallback(void *ptr)
{
  numpadButton('8');
}

void b9PopCallback(void *ptr)
{
  numpadButton('9');
}

void b0PopCallback(void *ptr)
{
  numpadButton('0');
}

void bAsteriskPopCallback(void *ptr)
{
  numpadButton('*');
}

void bNumberSignPopCallback(void *ptr)
{
  numpadButton('#');
}

void bDeletePopCallback(void *ptr)
{
  resetPasswordInput();
}

void resetPasswordInput()
{
  // reset password array
  for( int i = 0; i < sizeof(password);  ++i ){
    password[i] = (char)0; 
  }
   
  // reset text field in UI
  tPassword.setText("");

  // Reset counter
  counter = 0;
}

void setup(void) {    
//   Serial.begin(9600);
    
    // You might need to change NexConfig.h file in your ITEADLIB_Arduino_Nextion folder
    // Set the baudrate which is for debug and communicate with Nextion screen
    nexInit();

    // Register the pop/push event callback function of the components
    b1.attachPush(b1PopCallback, &b1);
    b2.attachPush(b2PopCallback, &b2);
    b3.attachPush(b3PopCallback, &b3);
    b4.attachPush(b4PopCallback, &b4);
    b5.attachPush(b5PopCallback, &b5);
    b6.attachPush(b6PopCallback, &b6);
    b7.attachPush(b7PopCallback, &b7);
    b8.attachPush(b8PopCallback, &b8);
    b9.attachPush(b9PopCallback, &b9);
    b0.attachPush(b0PopCallback, &b0);
    bAsterisk.attachPush(bAsteriskPopCallback, &bAsterisk);
    bNumberSign.attachPush(bNumberSignPopCallback, &bNumberSign);

    bDelete.attachPush(bDeletePopCallback, &bDelete);

    // initialize digital pin LED_BUILTIN as an output.
    // pinMode(LED_BUILTIN, OUTPUT);
    //static char password[5];
}

extern "C" void app_main(void) {  

    /*
    * When a pop or push event occured every time,
    * the corresponding component[right page id and component id] in touch event list will be asked.
    */
    setup();
    while(true)
    {
        nexLoop(nex_listen_list);
    }
}


// void app_main(void)
// {
    // printf("Hello world!\n");

    // /* Print chip information */
    // esp_chip_info_t chip_info;
    // esp_chip_info(&chip_info);
    // printf("This is %s chip with %d CPU cores, WiFi%s%s, ",
    //         CONFIG_IDF_TARGET,
    //         chip_info.cores,
    //         (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
    //         (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    // printf("silicon revision %d, ", chip_info.revision);

    // printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
    //         (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    // for (int i = 10; i >= 0; i--) {
    //     printf("Restarting in %d seconds...\n", i);
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }
    // printf("Restarting now.\n");
    // fflush(stdout);
    // esp_restart();
// }
